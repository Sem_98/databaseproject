package Stock;

public class Products_type {
    private static int id_type;
    private static String products_type;

    public static int getId_type() {
        return id_type;
    }

    public static void setId_type(int id_type) {
        Products_type.id_type = id_type;
    }

    public static String getProducts_type() {
        return products_type;
    }

    public static void setProducts_type(String products_type) {
        Products_type.products_type = products_type;
    }
}
