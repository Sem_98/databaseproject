package Stock;

public class Number {
    private static int id_number;
    private static int number;

    public static int getId_number() {
        return id_number;
    }

    public static void setId_number(int id_number) {
        Number.id_number = id_number;
    }

    public static int getNumber() {
        return number;
    }

    public static void setNumber(int number) {
        Number.number = number;
    }
}
