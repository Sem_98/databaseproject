package Stock;

public class Opt_cost {
    private static int id_optcost;
    private static int opt_cost;

    public static int getId_optcost() {
        return id_optcost;
    }

    public static void setId_optcost(int id_optcost) {
        Opt_cost.id_optcost = id_optcost;
    }

    public static int getOpt_cost() {
        return opt_cost;
    }

    public static void setOpt_cost(int opt_cost) {
        Opt_cost.opt_cost = opt_cost;
    }
}
