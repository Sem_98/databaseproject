package Stock;

public class Autorization {
     private static int id_autorization;
     private static String login;
     private static String password;


    public static int getId_autorization() {
        return id_autorization;
    }

    public static void setId_autorization(int id_autorization) {
        Autorization.id_autorization = id_autorization;
    }

    public static String getLogin() {
        return login;
    }

    public static void setLogin(String login) {
        Autorization.login = login;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Autorization.password = password;
    }
}
