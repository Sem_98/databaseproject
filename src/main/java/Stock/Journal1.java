package Stock;

public class Journal1 {
    private static int id;
    private static String products_name;
    private static String provider_name;
    private static String products_type;
    private static int id_optcost;
    private static int opt_cost;
    private static String date;
    private static int number;
    private static int opt_price;
    private static int id_number;
    private static int id_optprice;

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Journal1.id = id;
    }

    public static String getProducts_name() {
        return products_name;
    }

    public static void setProducts_name(String products_name) {
        Journal1.products_name = products_name;
    }

    public static String getProvider_name() {
        return provider_name;
    }

    public static void setProvider_name(String provider_name) {
        Journal1.provider_name = provider_name;
    }

    public static String getProducts_type() {
        return products_type;
    }

    public static void setProducts_type(String products_type) {
        Journal1.products_type = products_type;
    }

    public static int getId_optcost() {
        return id_optcost;
    }

    public static void setId_optcost(int id_optcost) {
        Journal1.id_optcost = id_optcost;
    }

    public static int getOpt_cost() {
        return opt_cost;
    }

    public static void setOpt_cost(int opt_cost) {
        Journal1.opt_cost = opt_cost;
    }

    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        Journal1.date = date;
    }

    public static int getNumber() {
        return number;
    }

    public static void setNumber(int number) {
        Journal1.number = number;
    }

    public static int getOpt_price() {
        return opt_price;
    }

    public static void setOpt_price(int opt_price) {
        Journal1.opt_price = opt_price;
    }

    public static int getId_number() {
        return id_number;
    }

    public static void setId_number(int id_number) {
        Journal1.id_number = id_number;
    }

    public static int getId_optprice() {
        return id_optprice;
    }

    public static void setId_optprice(int id_optprice) {
        Journal1.id_optprice = id_optprice;
    }
}
