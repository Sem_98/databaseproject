package Stock;

public class Products {
    private static int id_product;
    private static String product_name;

    public Products(int id_product, String product_name){
        this.id_product = id_product;
        this.product_name = product_name;
    }

    public Products(){

    }


    public static int getId_product() {
        return id_product;
    }

    public static void setId_product(int id_product) {
        Products.id_product = id_product;
    }

    public static String getProduct_name() {
        return product_name;
    }

    public static void setProduct_name(String product_name) {
        Products.product_name = product_name;
    }
}