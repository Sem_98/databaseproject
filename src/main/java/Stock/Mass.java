package Stock;

public class Mass {
    private static int id_mass;
    private static int mass;

    public static int getId_mass() {
        return id_mass;
    }

    public static void setId_mass(int id_mass) {
        Mass.id_mass = id_mass;
    }

    public static int getMass() {
        return mass;
    }

    public static void setMass(int mass) {
        Mass.mass = mass;
    }
}
