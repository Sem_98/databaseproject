package Stock;

public class Cost {
    private static int id_cost;
    private static int cost;


    public static int getId_cost() {
        return id_cost;
    }

    public static void setId_cost(int id_cost) {
        Cost.id_cost = id_cost;
    }

    public static int getCost() {
        return cost;
    }

    public static void setCost(int cost) {
        Cost.cost = cost;
    }

}
