package Stock;

public class Opt_price {
    private static int id_optprice;
    private static int opt_price;

    public static int getId_optprice() {
        return id_optprice;
    }

    public static void setId_optprice(int id_optprice) {
        Opt_price.id_optprice = id_optprice;
    }

    public static int getOpt_price() {
        return opt_price;
    }

    public static void setOpt_price(int opt_price) {
        Opt_price.opt_price = opt_price;
    }
}
