package Stock;

public class Price {
    private static int id_price;
    private static int price;

    public static int getId_price() {
        return id_price;
    }

    public static void setId_price(int id_price) {
        Price.id_price = id_price;
    }

    public static int getPrice() {
        return price;
    }

    public static void setPrice(int price) {
        Price.price = price;
    }
}
