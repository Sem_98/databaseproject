package Stock;

public class Provider {
    public static int id_provider;
    public static String provider_name;

    public Provider (int id_provider, String provider_name){
        this.id_provider = id_provider;
        this.provider_name = provider_name;
    }

    public Provider(){

    }

    public static int getId_provider(){
        return id_provider;
    }
    public static void setId_provider(int id_provider){
        Provider.id_provider = id_provider;
    }
    public static String getProvider_name(){
        return provider_name;
    }
    public static void setProvider_name(String provider_name){
        Provider.provider_name = provider_name;
    }
}
