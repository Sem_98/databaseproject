package Stock;

public class Journal {
    private static int id;
    private static int id_provider;
    private static int id_date;
    private static String products_name;
    private static String provider_name;
    private static int cost;
    private static String date;
    private static int mass;
    private static int price;
    private static int id_price;
    private static String products_type;
    private static int id_product;
    private static int id_mass;
    private static int id_cost;
    private static int id_type;

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Journal.id = id;
    }

    public static int getId_provider() {
        return id_provider;
    }

    public static void setId_provider(int id_provider) {
        Journal.id_provider = id_provider;
    }

    public static int getCost() {
        return cost;
    }

    public static void setCost(int cost) {
        Journal.cost = cost;
    }

    public static int getId_date() {
        return id_date;
    }

    public static void setId_date(int id_date) {
        Journal.id_date = id_date;
    }


    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        Journal.date = date;
    }

    public static String getProducts_name() {
        return products_name;
    }

    public static void setProducts_name(String products_name) {
        Journal.products_name = products_name;
    }

    public static String getProvider_name() {
        return provider_name;
    }

    public static void setProvider_name(String provider_name) {
        Journal.provider_name = provider_name;
    }

    public static int getMass() {
        return mass;
    }

    public static void setMass(int mass) {
        Journal.mass = mass;
    }


    public static int getPrice() {
        return price;
    }

    public static void setPrice(int price) {
        Journal.price = price;
    }

    public static int getId_price() {
        return id_price;
    }

    public static void setId_price(int id_price) {
        Journal.id_price = id_price;
    }


    public static String getProducts_type() {
        return products_type;
    }

    public static void setProducts_type(String products_type) {
        Journal.products_type = products_type;
    }

    public static int getId_product() {
        return id_product;
    }

    public static void setId_product(int id_product) {
        Journal.id_product = id_product;
    }

    public static int getId_mass() {
        return id_mass;
    }

    public static void setId_mass(int id_mass) {
        Journal.id_mass = id_mass;
    }

    public static int getId_cost() {
        return id_cost;
    }

    public static void setId_cost(int id_cost) {
        Journal.id_cost = id_cost;
    }

    public static int getId_type() {
        return id_type;
    }

    public static void setId_type(int id_type) {
        Journal.id_type = id_type;
    }
}
