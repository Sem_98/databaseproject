package DAO;

import Stock.Journal1;

import java.sql.SQLException;
import java.util.Vector;

public interface Journal1DAO {
    public void add(Journal1 journal1) throws SQLException;
    public Vector getAll() throws SQLException;
    public void update() throws SQLException;
    public void delete() throws SQLException;
}
