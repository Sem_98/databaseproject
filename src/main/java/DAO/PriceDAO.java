package DAO;

import Stock.Price;

import java.sql.SQLException;

public interface PriceDAO {
    public int add(Price price) throws SQLException;
}
