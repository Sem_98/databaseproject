package DAO;

import Stock.Provider;

import java.sql.SQLException;

public interface ProviderDAO {
    public int add(Provider provider) throws SQLException;
    public void update();
    public void delete();
}
