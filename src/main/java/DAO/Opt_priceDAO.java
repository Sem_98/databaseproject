package DAO;

import Stock.Opt_price;

import java.sql.SQLException;

public interface Opt_priceDAO {
    public int add(Opt_price opt_price) throws SQLException;
}
