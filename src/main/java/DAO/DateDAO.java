package DAO;

import Stock.Date;

import java.sql.SQLException;

public interface DateDAO {
    public int add(Date date) throws SQLException;
    public void update();
    public void delete();
}
