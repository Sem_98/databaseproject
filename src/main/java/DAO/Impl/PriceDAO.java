package DAO.Impl;

import Stock.Price;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PriceDAO implements DAO.PriceDAO{
    @Override
    public int add(Price price) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.price(price) VALUES (?)";
        String sqlRS = "Select * from products.price";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, price.getPrice());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_price");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }
}
