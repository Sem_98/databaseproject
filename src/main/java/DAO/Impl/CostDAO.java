package DAO.Impl;

import Stock.Cost;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CostDAO implements DAO.CostDAO {
    public int add(Cost cost) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.cost(cost) VALUES (?)";
        String sqlRS = "Select * from products.cost";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, cost.getCost());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_cost");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }

    public void update() {

    }

    public void delete() {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        String sql = "DELETE FROM products.cost";
        try {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
