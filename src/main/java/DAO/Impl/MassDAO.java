package DAO.Impl;

import Stock.Mass;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MassDAO implements DAO.MassDAO {
    @Override
    public int add(Mass mass) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.mass(mass) VALUES (?)";
        String sqlRS = "Select * from products.mass";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, mass.getMass());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_mass");
    }

} catch (SQLException e) {
        e.printStackTrace();
        } finally {
        if (statement != null) {
        statement.close();
        }
        if (connection != null) {
        connection.close();
        }
        }
        return id;
    }
}
