package DAO.Impl;


import Stock.Cost;
import Stock.Mass;
import Stock.Products;
import Stock.Products_type;
import com.mysql.Unit.Unit;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ProductsDAO implements DAO.ProductsDAO {
    public int add(Products products) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.products(products_name) VALUE (?)";
        String sqlRS = "Select * from products.products";
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, products.getProduct_name());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                id = resultSet.getInt("id_products");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
            //System.out.println(id);
        }
        return id;
    }


}
