package DAO.Impl;

import Stock.Opt_cost;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Opt_costDAO implements DAO.Opt_costDAO{
    @Override
    public int add(Opt_cost opt_cost) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.opt_cost(opt_cost) VALUES (?)";
        String sqlRS = "Select * from products.opt_cost";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, opt_cost.getOpt_cost());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_optcost");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }
}
