package DAO.Impl;

import Stock.Products_type;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Products_typeDAO implements DAO.Products_typeDAO{

    @Override
    public int add(Products_type products_type) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.products_type(products_type) VALUES (?)";
        String sqlRS = "Select * from products.products_type";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1, products_type.getProducts_type());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_type");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }
}

