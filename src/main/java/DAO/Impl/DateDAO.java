package DAO.Impl;

import Stock.Date;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DateDAO implements DAO.DateDAO{
    @Override
    public int add(Date date) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.date(date) VALUE (?)";
        String sqlRS = "Select * from products.date";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1, date.getDate());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_date");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
}
