package DAO.Impl;

import Stock.Journal;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class JournalDAO extends Vector implements DAO.JournalDAO{

    @Override
    public void add(Journal journal) throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Vector list = new Vector();
        String sql = "INSERT INTO products.journal(products_type, products_name, provider_name, cost, date, mass, price, id_type, id_products, id_provider, id_date, id_cost, id_mass, id_price) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1,journal.getProducts_type());
            statement.setString(2, journal.getProducts_name());
            statement.setString(3, journal.getProvider_name());
            statement.setInt(4, journal.getCost());
            statement.setString(5, journal.getDate());
            statement.setInt(6, journal.getMass());
            statement.setInt(7, journal.getPrice());
            statement.setInt(8, journal.getId_type());
            statement.setInt(9, journal.getId_product());
            statement.setInt(10, journal.getId_provider());
            statement.setInt(11, journal.getId_date());
            statement.setInt(12, journal.getId_cost());
            statement.setInt(13, journal.getId_mass());
            statement.setInt(14, journal.getId_price());
            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

    @Override
    public Vector getAll() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Vector list = new Vector();
        String sql = "SELECT *  FROM products.journal ORDER BY -id";
        try {
        statement = connection.prepareStatement(sql);
        resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Vector element = new Vector();
            int id = resultSet.getInt("id");
            String products_type = resultSet.getString("products_type");
            String products_name = resultSet.getString("products_name");
            String provider_name = resultSet.getString("provider_name");
            String date = resultSet.getString("date");
            int cost = resultSet.getInt("cost");
            int mass = resultSet.getInt("mass");
            int price = resultSet.getInt("price");

            element.add(id);
            element.add(products_type);
            element.add(products_name);
            element.add(provider_name);
            element.add(date);
            element.add(cost);
            element.add(mass);
            element.add(price);

            list.add(element);
        }

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        if (statement != null) {
            statement.close();
        }
        if (connection != null) {
            connection.close();
        }
    }
    System.out.println(list);
        return list;
    }

    @Override
    public void update() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        PreparedStatement statement1 = null;
        ResultSet resultSet = null;
        String sql = "UPDATE products.journal SET products_type=?, products_name=?, provider_name=?, date=?, cost=?, mass=?, price=? WHERE id=?";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1, Journal.getProducts_type());
            statement.setString(2, Journal.getProducts_name());
            statement.setString(3, Journal.getProvider_name());
            statement.setString(4, Journal.getDate());
            statement.setInt(5, Journal.getCost());
            statement.setInt(6, Journal.getMass());
            statement.setInt(7, Journal.getPrice());
            statement.setInt(8, Journal.getId());
            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void delete() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "delete from products.journal, products.provider, products.date, products.cost, products.mass, products.price using products.journal inner join products.provider on products.provider.id_provider = products.journal.id_provider inner join products.date on products.date.id_date = products.journal.id_date inner join products.cost on products.cost.id_cost = products.journal.id_cost inner join products.mass on products.mass.id_mass = products.journal.id_mass inner join products.price on products.price.id_price = products.journal.id_price where products.journal.id = ?";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, Journal.getId());
            statement.executeUpdate();



        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void select() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Vector list = new Vector();
        String sql = "SELECT *  FROM products.journal WHERE products_name = ?";
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, Journal.getProducts_name());
            resultSet = statement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
            }

}
