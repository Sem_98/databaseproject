package DAO.Impl;

import Stock.Opt_price;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Opt_priceDAO implements DAO.Opt_priceDAO{
    @Override
    public int add(Opt_price opt_price) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.opt_price(opt_price) VALUES (?)";
        String sqlRS = "Select * from products.opt_price";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, opt_price.getOpt_price());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_optprice");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }
}
