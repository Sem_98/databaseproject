package DAO.Impl;

import Stock.Autorization;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutorizationDAO implements DAO.AutorizationDAO{

    @Override
    public int select(String login, String password) throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        int id=0;
        PreparedStatement statement = null;
        ResultSet rs= null;
        String sql = "SELECT id_autorization FROM products.autorization WHERE login=? AND password=?";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1,login);
            statement.setString(2,password);
            rs=statement.executeQuery();
            while (rs.next()){
                id=rs.getInt("id_autorization");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }
}

