package DAO.Impl;

import Stock.Journal;
import Stock.Journal1;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class Journal1DAO implements DAO.Journal1DAO{
    @Override
    public void add(Journal1 journal1) throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Vector list = new Vector();
        String sql = "INSERT INTO products.journal1(products_type, products_name, provider_name, date, opt_cost, number, opt_price, id_optcost, id_number, id_optprice) VALUES (?,?,?,?,?,?,?,?,?,?)";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1,journal1.getProducts_type());
            statement.setString(2, journal1.getProducts_name());
            statement.setString(3, journal1.getProvider_name());
            statement.setString(4, journal1.getDate());
            statement.setInt(5, journal1.getOpt_cost());
            statement.setInt(6, journal1.getNumber());
            statement.setInt(7, journal1.getOpt_price());
            statement.setInt(8, journal1.getId_optcost());
            statement.setInt(9, journal1.getId_number());
            statement.setInt(10, journal1.getId_optprice());
            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public Vector getAll() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Vector list = new Vector();
        String sql = "SELECT *  FROM products.journal1 ORDER BY -id";
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Vector element = new Vector();
                int id = resultSet.getInt("id");
                String products_type = resultSet.getString("products_type");
                String products_name = resultSet.getString("products_name");
                String provider_name = resultSet.getString("provider_name");
                String date = resultSet.getString("date");
                int cost = resultSet.getInt("opt_cost");
                int mass = resultSet.getInt("number");
                int price = resultSet.getInt("opt_price");

                element.add(id);
                element.add(products_type);
                element.add(products_name);
                element.add(provider_name);
                element.add(date);
                element.add(cost);
                element.add(mass);
                element.add(price);

                list.add(element);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        System.out.println(list);
        return list;
    }

    @Override
    public void update() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        PreparedStatement statement1 = null;
        ResultSet resultSet = null;
        String sql = "UPDATE products.journal1 SET products_type=?, products_name=?, provider_name=?, date=?, opt_cost=?, number=?, opt_price=? WHERE id=?";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1, Journal1.getProducts_type());
            statement.setString(2, Journal1.getProducts_name());
            statement.setString(3, Journal1.getProvider_name());
            statement.setString(4, Journal1.getDate());
            statement.setInt(5, Journal1.getOpt_cost());
            statement.setInt(6, Journal1.getNumber());
            statement.setInt(7, Journal1.getOpt_price());
            statement.setInt(8, Journal1.getId());
            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void delete() throws SQLException {
        Connection connection;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "delete from products.journal1, products.opt_cost, products.number, products.opt_price using products.journal1 inner join products.opt_cost on products.opt_cost.id_optcost = products.journal1.id_optcost inner join products.number on products.number.id_number = products.journal1.id_number inner join products.opt_price on products.opt_price.id_optprice = products.journal1.id_optprice where id=?";
        try {
            statement=connection.prepareStatement(sql);
            statement.setInt(1, Journal1.getId());
            statement.executeUpdate();



        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}
