package DAO.Impl;

import Stock.Provider;
import com.mysql.Unit.Unit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProviderDAO implements DAO.ProviderDAO{

    @Override
    public int add(Provider provider) throws SQLException {
        Connection connection;
        int id = 0;
        Unit unit = new Unit();
        connection = unit.getConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO products.provider(provider_name) VALUE (?)";
        String sqlRS = "Select * from products.provider";
        try {
            statement=connection.prepareStatement(sql);
            statement.setString(1, provider.getProvider_name());
            statement.executeUpdate();

            statement = connection.prepareStatement(sqlRS);
            resultSet=statement.executeQuery();

            while (resultSet.next()){
                id = resultSet.getInt("id_provider");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return id;
    }


    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
}
