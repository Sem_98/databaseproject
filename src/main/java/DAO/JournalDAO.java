package DAO;

import Stock.Journal;

import java.sql.SQLException;
import java.util.Vector;

public interface JournalDAO {
    public void add(Journal journal) throws SQLException;
    public Vector getAll() throws SQLException;
    public void update() throws SQLException;
    public void delete() throws SQLException;
    public void select() throws SQLException;
}
