package DAO;

import java.sql.SQLException;

public interface AutorizationDAO {
    public int select(String login, String password)throws SQLException;
}
