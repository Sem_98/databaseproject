package DAO;

import Stock.Opt_cost;

import java.sql.SQLException;

public interface Opt_costDAO {
    public int add(Opt_cost opt_cost) throws SQLException;
}
