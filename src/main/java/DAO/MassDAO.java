package DAO;

import Stock.Mass;

import java.sql.SQLException;

public interface MassDAO {
    public int add(Mass mass) throws SQLException;
}
