package DAO;

import Stock.Number;

import java.sql.SQLClientInfoException;
import java.sql.SQLException;

public interface NumberDAO {
    public int add(Number number) throws SQLException;
}
