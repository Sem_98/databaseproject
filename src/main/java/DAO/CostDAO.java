package DAO;

import Stock.Cost;

import java.sql.SQLException;

public interface CostDAO {
    public int add(Cost cost) throws SQLException;
    public void update();
    public void delete();
}
