package com.mysql.Unit;

import Stock.Products;

import java.sql.*;

public class Unit {

    private static final String USERNAME = "root";
    private static final String PASSWORD = "vupidi23";
    private static final String URL = "jdbc:mysql://localhost:3306/mysql?userSSL=false";
    private Connection connection;

    public Connection getConnection() {
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            System.out.println("Connected");
        } catch (SQLException es) {
            System.out.println("Error connection!");
            es.printStackTrace();
        }
        return connection;
    }
}
