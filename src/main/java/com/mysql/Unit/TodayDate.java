package com.mysql.Unit;

import java.util.GregorianCalendar;

public class TodayDate {

    private static String year;
    private static String month;
    private static String day;

    private static String date;

    public static String getYear() {

        GregorianCalendar date = new GregorianCalendar();
        year = String.valueOf(date.get(GregorianCalendar.YEAR));
        return year;
    }

    public static String getMonth() {
        GregorianCalendar date = new GregorianCalendar();
        int m = date.get(GregorianCalendar.MONTH) + 1;
        month = String.valueOf(m);
        return month;
    }

    public static String getDay() {
        GregorianCalendar date = new GregorianCalendar();
        day = String.valueOf(date.get(GregorianCalendar.DAY_OF_MONTH));
        return day;
    }

    public static String getDate() {
        date = getYear() + "." + getMonth() + "." + getDay();
        return date;
    }
}
