package Service;

import DAO.Impl.PriceDAO;
import Stock.Price;

import java.sql.SQLException;

public class PriceDAOService {
    public int add(Price price) throws SQLException{
        PriceDAO priceDAO = new PriceDAO();
        return priceDAO.add(price);
    }
}
