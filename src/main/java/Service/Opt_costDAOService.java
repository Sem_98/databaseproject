package Service;

import DAO.Impl.CostDAO;
import DAO.Impl.Opt_costDAO;
import Stock.Cost;
import Stock.Opt_cost;

import java.sql.SQLException;

public class Opt_costDAOService {
    public int add(Opt_cost opt_cost) throws SQLException {
        Opt_costDAO opt_costDAO = new Opt_costDAO();
        return opt_costDAO.add(opt_cost);

    }
}
