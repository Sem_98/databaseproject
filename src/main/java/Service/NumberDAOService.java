package Service;

import DAO.Impl.NumberDAO;
import Stock.Number;

import java.sql.SQLException;

public class NumberDAOService {
    public int add(Number number) throws SQLException{
        NumberDAO numberDAO = new NumberDAO();
        return numberDAO.add(number);
    }
}
