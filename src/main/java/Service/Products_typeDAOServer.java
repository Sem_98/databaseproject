package Service;

import DAO.Impl.Products_typeDAO;
import Stock.Products_type;

import java.sql.SQLException;

public class Products_typeDAOServer {
    public int add(Products_type products_type)throws SQLException{
        Products_typeDAO products_typeDAO = new Products_typeDAO();
        return products_typeDAO.add(products_type);
    }
}
