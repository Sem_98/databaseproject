package Service;

import DAO.Impl.*;
import Stock.*;

import java.sql.SQLException;
import java.util.Vector;

public class Journal1DAOService {
    public void add(Journal1 journal1) throws SQLException {
        Journal1DAO journal1DAO = new Journal1DAO();
        journal1DAO.add(journal1);
    }
    public Vector getAll() throws SQLException {
        Journal1DAO journal1DAO=new Journal1DAO();
        return  journal1DAO.getAll();
    }
    public void update() throws SQLException {
        Journal1DAO journal1DAO = new Journal1DAO();
        journal1DAO.update();
    }
    public void delete() throws SQLException {
        Journal1DAO journal1DAO = new Journal1DAO();
        journal1DAO.delete();
    }
}
