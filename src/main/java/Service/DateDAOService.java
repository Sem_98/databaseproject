package Service;

import DAO.Impl.DateDAO;
import Stock.Date;

import java.sql.SQLException;

public class DateDAOService {
    public int add(Date date) throws SQLException {
        DateDAO dateDAO = new DateDAO();
        return dateDAO.add(date);
    }
}
