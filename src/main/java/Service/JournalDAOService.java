package Service;

import DAO.Impl.JournalDAO;
import Stock.Journal;

import java.sql.SQLException;
import java.util.Vector;

public class JournalDAOService {

    public void add(Journal journal) throws SQLException {
        JournalDAO journalDAO = new JournalDAO();
        journalDAO.add(journal);
    }
    public Vector getAll() throws SQLException {
        JournalDAO journalDAO=new JournalDAO();
        return  journalDAO.getAll();
    }
    public void update() throws SQLException {
        JournalDAO journalDAO = new JournalDAO();
        journalDAO.update();
    }
    public void delete() throws SQLException {
        JournalDAO journalDAO = new JournalDAO();
        journalDAO.delete();
    }

    public void select() throws SQLException {
        JournalDAO journalDAO=new JournalDAO();
        journalDAO.select();
    }

}
