package Service;

import DAO.Impl.MassDAO;
import Stock.Mass;

import java.sql.SQLException;

public class MassDAOService {
    public int add(Mass mass) throws SQLException {
        MassDAO massDAO = new MassDAO();
        return massDAO.add(mass);
    }
}
