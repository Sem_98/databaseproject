package Service;

import DAO.Impl.Opt_priceDAO;
import Stock.Opt_price;

import java.sql.SQLException;

public class Opt_priceDAOService {
    public int add(Opt_price opt_price) throws SQLException{
        Opt_priceDAO opt_priceDAO = new Opt_priceDAO();
        return opt_priceDAO.add(opt_price);
    }
}
