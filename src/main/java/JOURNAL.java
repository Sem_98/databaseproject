import Service.JournalDAOService;
import Stock.Journal;

import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Vector;

public class JOURNAL extends javax.swing.JFrame {



    public JOURNAL() throws SQLException {
        initComponents();
    }
    public static void add() throws SQLException {
        DefaultTableModel defaultTableModel = new DefaultTableModel();
        Vector colname = new Vector();
        Journal journal = new Journal();
        JournalDAOService journalDAOService= new JournalDAOService();
        colname.add("№");
        colname.add("Тип товару");
        colname.add("Назва товару");
        colname.add("Постачальник");
        colname.add("Дата постачання");
        colname.add("Ціна товару за 1 кг (грн)");
        colname.add("Маса товару (кг)");
        colname.add("Загальна ціна (грн)");

        Vector date = journalDAOService.getAll();
        journalDAOService.update();
        //System.out.println(date);

        defaultTableModel.setDataVector(date, colname);
        jTable1.setModel(defaultTableModel);
        jTable1.updateUI();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    public void initComponents() throws SQLException {

        jPanel3 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();


        jPanel3.setBackground(new java.awt.Color(255, 255, 174));
        Vector colname = new Vector();
        final JournalDAOService journalDAOService= new JournalDAOService();
        colname.add("№");
        colname.add("Тип товару");
        colname.add("Назва товару");
        colname.add("Постачальник");
        colname.add("Дата постачання");
        colname.add("Ціна товару за 1 кг (грн)");
        colname.add("Маса товару (кг)");
        colname.add("Загальна ціна (грн)");

        Vector date = journalDAOService.getAll();
        //System.out.println(date);


        final DefaultTableModel defaultTableModel = new DefaultTableModel(date, colname);
        jTable1 = new javax.swing.JTable(defaultTableModel);

        jScrollPane1.setViewportView(jTable1);

        jButton2.setBackground(new java.awt.Color(51, 51, 255));
        jButton2.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jButton2.setText("Редагувати");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
                int row = jTable1.getSelectedRow();
                String value = jTable1.getModel().getValueAt(row, 1).toString();
                String value1 = jTable1.getModel().getValueAt(row, 2).toString();
                String value2 = jTable1.getModel().getValueAt(row, 3).toString();
                String value3 = jTable1.getModel().getValueAt(row, 4).toString();
                String value4 = String.valueOf(Integer.valueOf(jTable1.getModel().getValueAt(row, 5).toString()));
                String value5 = String.valueOf(Integer.valueOf(jTable1.getModel().getValueAt(row, 6).toString()));
                String value6 = String.valueOf(Integer.valueOf(jTable1.getModel().getValueAt(row, 7).toString()));
                Journal.setProducts_type(value);
                Journal.setProducts_name(value1);
                Journal.setProvider_name(value2);
                Journal.setDate(value3);
                Journal.setCost(Integer.parseInt(value4));
                Journal.setMass(Integer.parseInt(value5));
                Journal.setPrice(Integer.parseInt(value6));
                Journal.setId(Integer.valueOf(jTable1.getModel().getValueAt(row, 0).toString()));
                Update update = new Update();
                update.setVisible(true);
            }
        });


        jButton3.setBackground(new java.awt.Color(51, 51, 255));
        jButton3.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jButton3.setText("Видалити");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
                JournalDAOService journalDAOService = new JournalDAOService();
                int row = jTable1.getSelectedRow();
                Journal.setId(Integer.valueOf(jTable1.getModel().getValueAt(row,0).toString()));
                try {
                    journalDAOService.delete();
                    add();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(143, 143, 143)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 199, Short.MAX_VALUE)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(147, 147, 147))
                        .addComponent(jScrollPane1)
        );
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(45, 45, 45))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }

    private void jButton3ActionPerformed(ActionEvent evt) {
    }


    public void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:

    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }


    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private static javax.swing.JTable jTable1;
    
}
